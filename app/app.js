const express = require('express');

const app = express();

const Product = require('./routes/product');

var usuariosRouter = require("./routes/Usuarios");

var tokenRouter = require("./routes/tokens");

var path = require('path');

const passport = require("./config/passport");

const session = require("express-session");

const cookieParser = require("cookie-parser");

var Usuario = require("./models/Usuario");

var Token = require("./models/Token");

const bcrypt = require("bcrypt");

var jwt = require("jsonwebtoken");

var authAPIRouter = require('./routes/auth');

let saltRounds = 10;

const store = new session.MemoryStore;

app.use(session({

    cookie: { magAge: 240 * 60 * 60 * 1000 }, //Tiempo en milisegundos

    store: store,

    saveUninitialized: true,



    resave: "true",



    secret: "517253712538"

}));

app.set('secretkey', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZTk0OGU0NmYwOWVhYzBhYTEyM2QyZiIsImlhdCI6MT');

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

//Nos permite manejar peticiones y enviar respuesta en formato json
app.use(express.json());
//De esta manera indicamos que no vamos a recibir peticiones enviadas directamente de un formulario, sino que sera todo enviado en json
app.use(express.urlencoded({ extended: false }));

app.use(express.static('public'));

app.use(cookieParser());

app.use(passport.initialize());

app.use(passport.session());

app.use("/token", tokenRouter);

app.use("/usuarios", usuariosRouter);

app.use('/product', loggedIn, Product);

app.use('/api/auth', authAPIRouter);

app.use("/api/product", validarUsuario, authAPIRouter);

app.use('/api/usuarios', authAPIRouter);

app.get("/login", function (req, res) {

    res.render("session/login");

});



app.post("/login", function (req, res, next) {

    passport.authenticate("local", function (err, usuario, info) {

        if (err) return next(err);

        if (!usuario) return res.render("session/login", { info });

        req.logIn(usuario, function (err) {

            if (err) return next(err);

            return res.redirect("/usuarios");

        });

    })(req, res, next);

});

app.get("/logout", function (req, res) {

    req.logOut(); //Limpiamos la sesión

    res.redirect("/");

});

app.get("/forgotPassword", function (req, res) {

    res.render("session/forgotPassword");

});

app.post("/forgotPassword", function (req, res) {

    Usuario.findOne({ email: req.body.email }, function (err, usuario) {

        if (!usuario) return res.render("session/forgotPassword", { info: { message: "No existe ese email en nuestra BBDD." } });



        usuario.resetPassword(function (err) {

            if (err) return next(err);

            console.log("session/forgotPasswordMessage");

        });

        res.render("session/forgotPasswordMessage");

    });

});

app.get("/resetPassword/:token", function (req, res, next) {

    Token.findOne({ token: req.params.token }, function (err, token) {

        if (!token) return res.status(400).send({ type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado." });



        Usuario.findById(token._userId, function (err, usuario) {

            if (!usuario) return res.status(400).send({ msg: "No existe un usuario asociado al token." });

            res.render("session/resetPassword", { errors: {}, usuario: usuario });

        });

    });

});





app.post("/resetPassword", function (req, res) {

    if (req.body.password != req.body.confirm_password) {

        res.render("session/resetPassword", {
            errors: { confirm_password: { message: "No coincide con el password introducido." } },

            usuario: new Usuario({ email: req.body.email })
        });

        return;

    }

    Usuario.findOne({ email: req.body.email }, function (err, usuario) {

        usuario.password = req.body.password,

            Usuario.findOneAndUpdate({ email: req.body.email }, { password: bcrypt.hashSync(usuario.password, saltRounds) }, function (err) {

                if (err) {

                    res.render("session/resetPassword", { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });

                } else {

                    res.redirect("/login");

                }

            });

    });

});



function loggedIn(req, res, next) {

    console.log(req.user);

    if (req.user) {

        next();

    } else {

        console.log("Usuario no logueado");

        res.redirect("/login");

    }

}


function validarUsuario(req, res, next) {

    console.log(req.app.get('secretkey'));
    console.log(req.headers);
    jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function (err, decoded) {

        if (err) {

            res.json({ status: "error", message: err.message, data: null });

        } else {

            req.body.userId = decoded.id;

            console.log('jwt verify: ' + decoded);

            next();

        }

    });

}



module.exports = app