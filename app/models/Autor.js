const mongoose = require('mongoose');

const autorSchema = new mongoose.Schema({
    nombre: String,
    biografia: String,
    fecha_nacimiento: Date,
    nacionalidad: String,
    imagen: String,
  });
  
  const Autor = mongoose.model('Autor', autorSchema);

  module.exports = Autor;