const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");
const crypto = require("crypto");
const mongoose = require('mongoose');
const Token = require("./Token");
const mailer = require("../mailer/mailer");

let validateEmail = function (email) {

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    return re.test(email);

}


let usuarioSchema = new mongoose.Schema({

    nombre: {

        type: String,

        trim: true,

        required: [true, "El nombre es obligatorio"]

    },

    email: {

        type: String,

        trim: true,

        required: [true, "El email es obligatorio"],

        lowercase: true,

        unique: true,

        validate: [validateEmail, "Por favor, introduzca un email válido"],

        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/]

    },

    password: {

        type: String,

        required: [true, "El password es obligatorio"]

    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verificado: {

        type: Boolean,

        default: false

    }

});

let saltRounds = 10;


// let validateEmail = function (email) {

//     let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

//     return re.test(email);

// }


usuarioSchema.methods.validPassword = function (password) {

    return bcrypt.compareSync(password, this.password);

}

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {

    let token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") }); //El token es un String en hexadecimal

    let email_destination = this.email;

    token.save(function (err) {

        if (err) { return console.log(err.message); }



        let mailOptions = {

            from: "no-reply@redbicicletas.com",

            to: email_destination,

            subject: "Verificación de cuenta",

            text: "Hola,\n\n" + "Por favor, para verificar su cuenta haga click en este enlace: \n" + "http://localhost:3000" + "\/token/confirmation\/" + token.token + ".\n"

        };



        mailer.sendMail(mailOptions, function (err) {

            if (err) { return console.log(err.message); }

            console.log("Se ha enviado un email de bienvenida a " + email_destination + ".");

        });

    });

};

usuarioSchema.methods.resetPassword = function (cb) {

    let token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") }); //El toke es un String en hexadecimal

    let email_destination = this.email;

    token.save(function (err) {

        if (err) { return cb(err); }



        let mailOptions = {

            from: "no-reply@redbicicletas.com",

            to: email_destination,

            subject: "Reseteo de password de cuenta",

            text: "Hola,\n\n" + "Por favor, para resetear el password de su cuenta haga click en este enlace: \n" + "http://localhost:3000" + "\/resetPassword\/" + token.token + ".\n"

        };



        mailer.sendMail(mailOptions, function (err) {

            if (err) { return cb(err); }



            console.log("Se ha enviado un email para resetear el password a: " + email_destination + ".");



        });

        cb(null);

    });

};

usuarioSchema.plugin(uniqueValidator, { message: "El email ya existe con otro usuario." });

usuarioSchema.pre("save", function (next) {

    if (this.isModified("password")) {

        this.password = bcrypt.hashSync(this.password, saltRounds);

    }

    next();

});

module.exports = mongoose.model("Usuario", usuarioSchema);


