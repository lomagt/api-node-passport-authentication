const mongoose = require('mongoose');
//const Autor = mongoose.model('Autor');

const libroSchema = new mongoose.Schema({
    titulo: String,
    paginas: Number,
    isbn: String,
    autor: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: "Autor",
    }
});

const Libro = mongoose.model('Libro', libroSchema);

module.exports = Libro;