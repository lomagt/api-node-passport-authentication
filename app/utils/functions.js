

function header() {
  let html;
  html = `
        <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <link rel="stylesheet" href="/bootstrap-5.1.3-dist/css/bootstrap.min.css">
                    <title>MVCNodeJS</title>
                </head>
                <body>
                <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-2">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="#">NodeJS-MVC</a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarTogglerDemo02"
                        aria-controls="navbarTogglerDemo02"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="http://localhost:3000/contacto">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="http://localhost:3000/formulario">Insertar Autor</a>
                            </li>
                            
                        </ul>
                        <form class="d-flex">
                            
                            
                        </form>
                    </div>
                </div>
            </nav>
            <div class="container">

              <div class="row">
    `;
  return html;
}

function footer() {
  let html;
  html = `
              </div>
            </div>
            <footer class="d-flex justify-content-center align-items-center bg-dark text-white mt-3">
                <p>Creado por Manuel Mañas Alfaro</p>
            </footer>
            <script src="/bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>
        </body>
        </html>
    `;
  return html;
}

function form() {
  let html;
  html = `
    <div class="container-fluid mt-3">
    <form action="http://localhost:3000/insertar" method="post" enctype=multipart/form-data>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" placeholder="name" name="name">
        <label for="floatingInput">Nombre</label>
      </div>
      <div class="form-floating mb-3">
        <textarea class="form-control" placeholder="Biografia" id="floatingTextarea2" style="height: 100px" name="biografia"></textarea>
        <label for="floatingTextarea2">Biografía</label>
      </div>
      <div class="form-floating mb-3">
        <input type="date" class="form-control" id="floatingNacimiento" placeholder="Biografía" name="nacimiento">
        <label for="floatingNacimiento">Fecha Nacimiento</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingNacion" placeholder="Biografía" name="nacion">
        <label for="floatingNacion">Nacionalidad</label>
      </div>
      <div class="form-floating mb-3">
        <input type="file" class="form-control" id="floatingFile" placeholder="Imagen" name="file">
        <label for="floatingFile">Insertar Imagen</label>
      </div>
      <button class="btn btn-outline-info p-2 mt-3" type="submit">Insertar</button>
    </form>
  </div>
    `;
  return html;
}

function card(autor) {

  const options = { day: 'numeric', month: 'long', year: 'numeric' };

  let html;
  html = `
    <div class="col-6 mt-3">
    <div class="card">
    <h5 class="card-header">Autor</h5>
    <img src="/uploads/${autor.imagen}" class="card-img-top" alt="..." width="100%" height="350px">
    <div class="card-body">
      <h5 class="card-title">${autor.nombre}</h5>
      <p class="card-text">${autor.biografia}</p>
      <p class="text-center">Nacionalidad: ${autor.nacionalidad}</p>
      <p class="text-center">Fecha De Nacimiento: ${autor.fecha_nacimiento.toLocaleDateString('es-ES', options)}</p>
      <a href="http://localhost:3000/libros/${autor.id}" class="btn btn-primary">Ver Libros</a>
      <a href="http://localhost:3000/formulario2/${autor.id}" class="btn btn-success">Añadir Libro</a>
      <a href="http://localhost:3000/eliminarAutor/${autor.id}" class="btn btn-danger">Eliminar Autor</a>
      <a href="http://localhost:3000/formActualizarAutor/${autor.id}" class="btn btn-warning">Actualizar Autor</a>
    </div>
  </div>
  </div>
    `;

  return html;
}

function form2(id) {
  let html;
  html = `
    <div class="container-fluid mt-3">
      <form action="http://localhost:3000/incluir" method="post">
        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="floatingInput" placeholder="name" name="titulo">
          <label for="floatingInput">Título</label>
        </div>
        <div class="form-floating mb-3">
          <input type="number" class="form-control" id="floatingPag" placeholder="Páginas" name="paginas">
          <label for="floatingPag">Páginas</label>
        </div>
        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="floatingisbn" placeholder="ISBN" name="isbn">
          <label for="floatingisbn">ISBN</label>
        </div>
        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="floatingNacion" placeholder="Autor ID" name="id" value="${id}">
          <label for="floatingNacion">Autor ID</label>
        </div>
        <button class="btn btn-outline-info p-2 mt-3" type="submit">Insertar</button>
      </form>
    </div>
    `;
  return html;
}

function cardLibro(libro) {

  let html;
  html = `
  <div class="col-4">
    <div class="card">
    <h5 class="card-header">Autor</h5>
    <div class="card-body">
      <h5 class="card-title">${libro.titulo}</h5>
      <p class="card-text">ISBN: ${libro.isbn}</p>
      <p class="text-center">Nº Páginas: ${libro.paginas}</p>
      <a href="http://localhost:3000/eliminarLibro/${libro.id}" class="btn btn-primary">Eliminar Libro</a>
    </div>
  </div>
  </div>
    `;

  return html;
}

function form3(params) {

  let fechaNormal = params.fecha_nacimiento.getFullYear();
  fechaNormal +='/'+ (params.fecha_nacimiento.getMonth()+1);
  fechaNormal +='/'+ (params.fecha_nacimiento.getDate());
  
  console.log(fechaNormal);
  let html;
  html = `
  <div class="container-fluid mt-3">
  <form action="http://localhost:3000/updateautor" method="POST">
    <div class="form-floating mb-3">
      <input type="text" class="form-control" id="floatingInput" placeholder="name" name="name" value="${params.nombre}">
      <label for="floatingInput">Nombre</label>
    </div>
    <div class="form-floating mb-3">
      <textarea class="form-control" placeholder="Biografia" id="floatingTextarea2" style="height: 100px" name="biografia">
      ${params.biografia}
      </textarea>
      <label for="floatingTextarea2">Biografía</label>
    </div>
    <div class="form-floating mb-3">
      <input type="text" class="form-control" id="floatingNacimiento" placeholder="Fecha_nacimiento" name="nacimiento" value="${fechaNormal}">
      <label for="floatingNacimiento">Fecha Nacimiento</label>
    </div>
    <div class="form-floating mb-3">
      <input type="text" class="form-control" id="floatingNacion" placeholder="Nacionalidad" name="nacion" value="${params.nacionalidad}">
      <label for="floatingNacion">Nacionalidad</label>
    </div>
    <input type="hidden" class="form-control" id="floatinghidden" placeholder="id" name="id" value="${params.id}">
    <button class="btn btn-outline-warning p-2 mt-3" type="submit">Modificar</button>
  </form>
</div>
  `;
  return html;
}

module.exports = {
  header,
  footer,
  form,
  card,
  form2,
  cardLibro,
  form3
}