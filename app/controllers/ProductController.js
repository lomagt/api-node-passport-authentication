const Autor = require('../models/Autor');
const Libro = require('../models/Libro');
const funciones = require('../utils/functions');
const fs = require('fs');
const validator = require('validator');
var path = require('path');
const Usuario = require('../models/Usuario');


function contactos(req, res, next) {

  Autor.find({}, function (err, doc) {
    if (err) {
      console.log("El Find ha fallado");
    } else {

      let html = funciones.header();

      for (let key of doc) {
        if (key === undefined) {

        } else {
          html += funciones.card(key);
        }
      }

      html += funciones.footer();

      res.send(html);
    }
  });
}

function insertar(req, res) {
  let campos = req.body;

  // Conseguir nombre y la extensión del archivo
  var file_path = req.files.file.path;
  
  var file_split = file_path.split('\\');

  // * ADVERTENCIA * EN LINUX O MAC
  //var file_split = file_path.split('/');

  // Nombre del archivo
  var file_name = file_split[2];
  
  // Extension del fichero
  var extensión_split = file_name.split('\.');
  var file_ext = extensión_split[1];

  // Comprobar la extensión, solo imagenes, si es valida borrar el fichero
  if (file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'gif') {

    // Borrar el archivo subido
    fs.unlink(file_path, (err) => {
      return res.status(200).send({
        status: 'error',
        message: 'La extension de la imagen no es valida'
      });
    });

  } else {

    const autor = new Autor({
      nombre: campos.name,
      biografia: campos.biografia,
      fecha_nacimiento: campos.nacimiento,
      nacionalidad: campos.nacion,
      imagen: file_name,
    });
    console.log(autor);

    autor.save((err, user) => {
      if (err || !user) {
        return res.status(404).send({
          status: 'Error',
          message: 'No se puede guardar el Formulario'
        });
      }
      /*
      return res.status(200).send({
        status: 'success',
        user
      });    
      */

      res.redirect('./contacto');

    });
  }
}

function incluir(req, res) {
  let campos = req.body;

  const libro = new Libro({
    titulo: campos.titulo,
    paginas: campos.paginas,
    isbn: campos.isbn,
    autor: campos.id
  });
  console.log(libro);

  libro.save((err, user) => {
    if (err || !user) {
      return res.status(404).send({
        status: 'Error',
        message: 'No se puede guardar el Formulario'
      });
    }
    /*
    return res.status(200).send({
      status: 'success',
      user
    });    
    */

    res.redirect('./contacto');

  });
}

function formulario(req, res) {
  let html = funciones.header();

  html += funciones.form();

  html += funciones.footer();

  res.send(html);
}

function formulario2(req, res) {

  let html = funciones.header();

  html += funciones.form2(req.params.id);

  html += funciones.footer();

  res.send(html);
}

function libros(req, res) {

  Libro.find({ autor: req.params.id }, function (err, doc) {
    if (err) {
      console.log("El Find ha fallado");
    } else {

      let html = funciones.header();

      for (let key of doc) {
        html += funciones.cardLibro(key);
      }

      html += funciones.footer();

      res.send(html);
    }
  });
}

function eliminarLibro(req, res) {
  Libro.deleteOne({ _id: req.params.id }, (err) => {
    if (err) {
      return res.status(404).send({
        status: 'Error',
        message: 'No se puede borrar el elmento'
      });
    }

    // return res.status(200).send({
    //   status: 'success',
    //   delElemt
    // });

    res.redirect('/product/contacto');
  });
}

function eliminarAutor(req, res) {
  Autor.deleteOne({ _id: req.params.id }, (err) => {
    if (err) {
      return res.status(404).send({
        status: 'Error',
        message: 'No se puede borrar el elmento'
      });
    }

  });
  Libro.deleteMany({ autor: req.params.id }, (err) => {
    if (err) {
      return res.status(404).send({
        status: 'Error',
        message: 'No se puede borrar el elmento'
      });
    }

    res.redirect('/product/contacto');
  });
}

function formActualizarAutor(req, res) {

  Autor.find({ _id: req.params.id }, (err, upElement) => {
    if (err || !upElement) {
      return res.send({ message: 'Error o no existe el contacto' });
    } else {

      let html = funciones.header();

      html += funciones.form3(upElement[0]);

      html += funciones.footer();

      res.send(html);

    }
  });

}

function updateAutor(req, res) {
  let autor = req.body;
  console.log(autor);

  Autor.updateOne({ _id: autor.id }, { nombre: autor.name, biografia: autor.biografia, fecha_nacimiento: autor.nacimiento, nacionalidad: autor.nacion }, (err, contact) => {
    if (err) {
      return 'No se ha actualizado';
    } else {
      res.redirect('/product/contacto');
    }
  });
}

function validar(req, res, next){
  if(validator.isEmpty(req.body.name)){
    res.redirect('/product/formulario');
  }else{
    next();
  }
}

module.exports = {
  contactos,
  formulario,
  insertar,
  incluir,
  formulario2,
  libros,
  eliminarLibro,
  eliminarAutor,
  formActualizarAutor,
  updateAutor,
  validar
};