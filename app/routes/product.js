const express = require('express');
const ProductController = require('../controllers/ProductController');

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir:'public/uploads'});

const router = express.Router();

router.get('/contacto', ProductController.contactos)
      .get('/formulario', ProductController.formulario)
      .post('/insertar', md_upload, ProductController.validar, ProductController.insertar)
      .get('/formulario2/:id', ProductController.formulario2)
      .post('/incluir', ProductController.incluir)
      .get('/libros/:id', ProductController.libros)
      .get('/eliminarLibro/:id',ProductController.eliminarLibro)
      .get('/eliminarAutor/:id',ProductController.eliminarAutor)
      .get('/formActualizarAutor/:id',ProductController.formActualizarAutor)
      .post('/updateautor',ProductController.updateAutor)
      
module.exports = router;